<?php


class ImageUploader
{
    protected $filePath;
    protected $errorCode;
    protected $limitBytes = 1024 * 1024 * 5;
    protected $limitWidth = 1280;
    protected $limitHeight = 1280;
    protected $imagePathString = 'pics/';

    public function __construct($filePath, $errorCode)
    {
        $this->filePath = $filePath;
        $this->errorCode = $errorCode;
    }

    public function getImagePath()
    {
        if (is_uploaded_file($this->filePath)) {

            if ($this->errorCode !== UPLOAD_ERR_OK) {

                $errorMessages = [
                    UPLOAD_ERR_INI_SIZE => 'Размер файла превысил значение upload_max_filesize в конфигурации PHP.',
                    UPLOAD_ERR_FORM_SIZE => 'Размер загружаемого файла превысил значение MAX_FILE_SIZE в HTML-форме.',
                    UPLOAD_ERR_PARTIAL => 'Загружаемый файл был получен только частично.',
                    UPLOAD_ERR_NO_FILE => 'Файл не был загружен.',
                    UPLOAD_ERR_NO_TMP_DIR => 'Отсутствует временная папка.',
                    UPLOAD_ERR_CANT_WRITE => 'Не удалось записать файл на диск.',
                    UPLOAD_ERR_EXTENSION => 'PHP-расширение остановило загрузку файла.',
                ];

                $unknownMessage = 'При загрузке файла произошла неизвестная ошибка.';

                if (isset ($errorMessages[$this->errorCode])) {
                    $outputMessage = $errorMessages[$this->errorCode];
                } else {
                    $outputMessage = $unknownMessage;
                }

                die($outputMessage);
            }

            $fileInfo = finfo_open(FILEINFO_MIME_TYPE);

            $mimeType = (string)finfo_file($fileInfo, $this->filePath);

            if (strpos($mimeType, 'image') === false) {
                die('Можно загружать только изображения.');
            }
            $image = getimagesize($this->filePath);

            if (filesize($this->filePath) > $this->limitBytes) {
                die('Размер изображения не должен превышать 5 Мбайт.');
            }
            if ($image[1] > $this->limitHeight) {
                die('Высота изображения не должна превышать 768 точек.');
            }
            if ($image[0] > $this->limitWidth) {
                die('Ширина изображения не должна превышать 1280 точек.');
            }

            $imageName = md5_file($this->filePath);

            $extension = image_type_to_extension($image[2]);

            $format = str_replace('jpeg', 'jpg', $extension);

            $this->imagePathString .= $imageName . $format;

            if (!move_uploaded_file($this->filePath, $this->imagePathString)) {
                die('При записи изображения на диск произошла ошибка.');
            }
        } else {
            return 'img/noimage.png';
        }
        return $this->imagePathString;
    }

}