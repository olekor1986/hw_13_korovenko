<?php


class Gadget
{
    protected $id;
    protected $title;
    protected $price;
    protected $description;
    protected $type;
    protected $image_file_path = 'img/noimage.png';

    public function __construct($title, $price, $description, $type, $image_file_path)
    {
        $this->title = $title;
        $this->price = $price;
        $this->description = $description;
        $this->type = $type;
        $this->image_file_path = $image_file_path;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getImageFilePath()
    {
        return $this->image_file_path;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    static public function showAll(PDO $pdo)
    {
        try {
            $sql = "SELECT * FROM gadgets";
            $result = $pdo->query($sql);
            $gadgetsArray = $result->fetchAll();
            $gadgetObj = [];
            foreach ($gadgetsArray as $key => $value) {
                $gadgetObj[] = new Gadget($value['title'], $value['price'], $value['description'],
                    $value['type'], $value['image_file_path']);
                $gadgetObj[$key]->setId($value['id']);
            }
            return $gadgetObj;
        } catch (Exception $e) {
            $message = "Ошибка получения данных. " . $e->getMessage();
            echo $message;
        }
    }
    static public function showById($id, PDO $pdo)
    {
        try {
            $sql = "SELECT * FROM gadgets WHERE id = :id";
            $selectObj = $pdo->prepare($sql);
            $selectObj->bindValue(':id', $id);
            $selectObj->execute();
            $gadget = $selectObj->fetch();
            $gadgetObj = new Gadget($gadget['title'], $gadget['price'], $gadget['description'],
                $gadget['type'], $gadget['image_file_path']);
            $gadgetObj->setId($id);
            return $gadgetObj;
        } catch (Exception $e) {
            $message = "Ошибка получения данных. " . $e->getMessage();
            die($message);
        }
    }
    public function addNewGadget(PDO $pdo)
    {
        try {
            $sql = "INSERT INTO gadgets SET
                    title = :title,
                    price = :price,
                    description = :description,
                    type = :type,
                    image_file_path = :image_file_path";
            $newGadgetObj = $pdo->prepare($sql);
            $newGadgetObj->bindValue(':title', $this->title);
            $newGadgetObj->bindValue(':price', $this->price);
            $newGadgetObj->bindValue(':description', $this->description);
            $newGadgetObj->bindValue(':type', $this->type);
            $newGadgetObj->bindValue(':image_file_path', $this->image_file_path);
            $newGadgetObj->execute();
        } catch (Exception $e) {
            $message = "Ошибка сохранения данных. " . $e->getMessage();
            die($message);
        }
    }
    public function updateGadget(PDO $pdo)
    {
        try {
            $sql = "UPDATE gadgets SET
                    title = :title,
                    price = :price,
                    description = :description,
                    type = :type,
                    image_file_path = :image_file_path
                    WHERE id = :id";
            $newGadgetObj = $pdo->prepare($sql);
            $newGadgetObj->bindValue(':id', $this->id);
            $newGadgetObj->bindValue(':title', $this->title);
            $newGadgetObj->bindValue(':price', $this->price);
            $newGadgetObj->bindValue(':description', $this->description);
            $newGadgetObj->bindValue(':type', $this->type);
            $newGadgetObj->bindValue(':image_file_path', $this->image_file_path);
            $newGadgetObj->execute();
        } catch (Exception $e) {
            $message = "Ошибка обновления данных. " . $e->getMessage();
            die($message);
        }
    }
    static public function deleteGadget($id, PDO $pdo)
    {
        try {
            $sql = "DELETE FROM gadgets WHERE id = :id";
            $selectObj = $pdo->prepare($sql);
            $selectObj->bindValue(':id', $id);
            $selectObj->execute();
        } catch (Exception $e) {
            $message = "Ошибка удаления данных. " . $e->getMessage();
            die($message);
        }
    }
}