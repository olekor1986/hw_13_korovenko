<?php
require_once "config/db_config.php";
require_once "Classes/Gadget.php";
require_once "Classes/ImageUploader.php";

$filePath  = htmlspecialchars($_FILES['upload']['tmp_name'], ENT_QUOTES, 'UTF-8');
$errorCode = $_FILES['upload']['error'];

$image = new ImageUploader($filePath, $errorCode);

$imagePathString = $image->getImagePath();

$gadgetTitle = htmlspecialchars(stripslashes(trim($_POST['title'])), ENT_QUOTES, "UTF-8");
$gadgetPrice = htmlspecialchars(stripslashes(trim($_POST['price'])), ENT_QUOTES, "UTF-8");
$gadgetDescription = htmlspecialchars(stripslashes(trim($_POST['description'])), ENT_QUOTES, "UTF-8");
$gadgetType = htmlspecialchars(stripslashes(trim($_POST['type'])), ENT_QUOTES, "UTF-8");

$gadgetObj = new Gadget($gadgetTitle, $gadgetPrice, $gadgetDescription, $gadgetType, $imagePathString);

$gadgetObj->addNewGadget($db);
header("Location:index.php");
?>